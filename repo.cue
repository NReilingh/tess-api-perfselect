// https://repo.fyi

provides: {
    path: "main.js"
    description: "Quasi-interactive CLI script"
    targets: {
        description: "Node.js"
        exec: "node main.js"
    }
    expects: env: {
        name: "TESS_API_AUTH"
        description: "Tessitura auth string in the form username:usergroup:location:password"
    }
}
