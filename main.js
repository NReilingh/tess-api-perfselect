// Pure Node.js CLI implementation.
// Configure with environment variables listed below,
// and interact by supplying CLI arguments as directed by output.

// Links are provided to documentation for each endpoint at two sources:
// * tessituranetwork.com documentation can be accessed with a network developer login, see https://tessituranetwork.com/devs
// * tess-svc-test.fc.bard.edu documentation can be accessed by proxying through Barrel's public IP.
const https = require('https');

// Configuration
// This works when network firewall access to the services is allowed.
// If using a dynamic IP, we may need to reverse-proxy the service and throw on an additional auth header to the HTTP requests.
// Working on this...
const SERVICE_URL = process.env.TESS_API_URL || 'https://tess-svc-test.fc.bard.edu/TessituraService/';
const SERVICE_AUTH = process.env.TESS_API_AUTH; // 'username:usergroup:location:password'

// Parse command-line arguments in key=value form.
const { season, prod_season } = Object.fromEntries(
    process.argv.slice(2)
        .filter(e => e.indexOf('=') > -1)
        .map(e => e.split('=').slice(0, 2))
);
console.log("Received input, season:", season, "prod_season:", prod_season);

// Implement a minimal API client
// All of these endpoints are GET REST methods with occasional URL parameters.
function client(route, params) {
    const { hostname, pathname } = new URL(SERVICE_URL);
    return new Promise((resolve, reject) => {
        https.get(
            {
                hostname,
                path: `${pathname}${route}?${new URLSearchParams(params).toString()}`,
                auth: SERVICE_AUTH,
                headers: {
                    Accept: 'application/json'
                }
            },
            res => {
                if (res.statusCode !== 200) {
                    reject(res.statusCode);
                    return;
                }
                res.setEncoding("utf8");
                let body = "";
                res.on("data", data => {
                    body += data;
                });
                res.on("end", () => {
                    resolve(JSON.parse(body));
                });
            }
        );
    });
}

// Top-level async IIFE
(async function () {
    // Switch based on what parameters are supplied
    // The cases are ordered from most-specific to least-specific -- read in reverse order starting from default.
    switch (true) {
        case (!!prod_season):
            console.log(`prod season ${prod_season} supplied -- fetching performance list`);
            // Fetch results from the TXN/Performances endpoint.
            // Docs here: https://www.tessituranetwork.com/REST_v151/TessituraService/HELP/API/GET_TXN_PERFORMANCES_PERFORMA_6.HTM
            // or here: https://tess-svc-test.fc.bard.edu/TessituraService/Help/Api/GET-TXN-Performances_performanceIds_seasonIds_productionSeasonId
            try {
                const performances = await client('TXN/Performances', { productionSeasonId: prod_season });
                /* Returns an array of Performance objects. This object signature is massive,
                but the few properties we care about are the most stable ones here. Example JSON:
                {
                    "AvailSaleIndicator": true,
                    "BestSeatMap": {
                        "Id": 60,
                        "Description": "GA Pods",
                        "GaIndicator": "N",
                        "IsGA": false
                    },
                    "BudgetAmount": null,
                    "Campaign": {
                        "AutoRestrictContributionsBeforeStartDate": false,
                        "Type": "C",
                        "Id": 307,
                        "CategoryId": 2,
                        "ControlGroup": {
                            "Description": "(Default Control Group)",
                            "Id": -1,
                            "Inactive": false
                        },
                        "CreatedDateTime": "2021-01-20T15:38:25.497-05:00",
                        "CreateLocation": "multimaia_2",
                        "CreatedBy": "mkaufman",
                        "Description": "2021 Summerscape",
                        "EndDateTime": "2022-06-30T23:59:59.997-04:00",
                        "EventDateTime": null,
                        "EventExpense": null,
                        "EventMinimumAmount": null,
                        "FYear": 2022,
                        "GiftAmount": 0.0,
                        "GoalAmount": null,
                        "Inactive": false,
                        "UpdatedDateTime": "2021-01-20T15:38:41.123-05:00",
                        "UpdatedBy": "mkaufman",
                        "MembershipIncludesMatchingGifts": "2",
                        "MinimumPledgeAmount": null,
                        "Notes": null,
                        "NumSuccessCust": 2,
                        "NumTargetCust": 0,
                        "NumTickets": null,
                        "PledgeReceivedAmount": 0.0,
                        "PledgeAmount": 0.0,
                        "SingleOpenPledgeIndicator": null,
                        "StartDateTime": "2021-01-01T00:00:00-05:00",
                        "Status": "A",
                        "TotalTicketIncome": 105.0,
                        "EditIndicator": false
                    },
                    "DefaultEndSaleDateTime": "2021-07-17T19:30:00-04:00",
                    "DefaultStartSaleDateTime": "2021-05-20T00:00:00-04:00",
                    "DoorsClose": null,
                    "DoorsOpen": null,
                    "Duration": null,
                    "Facility": {
                        "Description": "Montgomery Place Pod",
                        "Id": 53,
                        "SeatMap": {
                            "Description": "Montgomery Place Pod",
                            "Id": 62
                        }
                    },
                    "Code": "0716JVB",
                    "Date": "2021-07-16T19:30:00-04:00",
                    "Id": 2478,
                    "Status": {
                        "Id": 1,
                        "Description": "On Sale"
                    },
                    "Type": {
                        "Id": 6,
                        "Description": "Performance",
                        "Inactive": false
                    },
                    "ProductionSeason": {
                        "Production": {
                            "Id": 2473,
                            "Description": "Montgomery Place Stage"
                        },
                        "Id": 2474,
                        "Description": "Justin Vivian Bond",
                        "Season": {
                            "Id": 88,
                            "Description": "2021 Summer",
                            "FYear": 2022,
                            "Inactive": false
                        }
                    },
                    "PublishClientEndDate": null,
                    "PublishClientStartDate": null,
                    "PublishWebApiEndDate": null,
                    "PublishWebApiStartDate": null,
                    "RankType": {
                        "Id": 1,
                        "Description": "Default TR_RANK_TYPE",
                        "Inactive": false,
                        "ControlGroup": {
                            "Description": "(Default Control Group)",
                            "Id": -1,
                            "Inactive": false
                        }
                    },
                    "SalesNotes": null,
                    "SalesNotesRequired": false,
                    "Season": {
                        "Id": 88,
                        "Description": "2021 Summer",
                        "FYear": 2022,
                        "Inactive": false
                    },
                    "TimeSlot": {
                        "Id": 1,
                        "Description": "Evening",
                        "Inactive": false
                    },
                    "TvIndicator": null,
                    "ZoneMap": {
                        "Description": "MP Stage Pods",
                        "Id": 127,
                        "Inactive": false,
                        "SeatMap": {
                            "Description": "Montgomery Place Pod",
                            "Id": 62
                        }
                    },
                    "CreatedDateTime": "2021-05-20T16:09:15.067-04:00",
                    "CreateLocation": "multimaia_1",
                    "CreatedBy": "mkaufman",
                    "UpdatedDateTime": "2021-05-20T16:09:15.987-04:00",
                    "UpdatedBy": "mkaufman",
                    "EditIndicator": false,
                    "Description": "Justin Vivian Bond",
                    "ShortName": "Justin Vivian Bond",
                    "Text1": null,
                    "Text2": null,
                    "Text3": null,
                    "Text4": null
                } */
                console.log("Save the selected performance ID to the event.");
                performances.forEach(({ Id, Code, Description }) => {
                    console.log(`${Id}: ${Code} - ${Description}`);
                });
                console.log("Save the selected performance ID to the event.");
            } catch (error) {
                console.error(error);
            }
            break;
        case (!!season):
            console.log(`season ${season} supplied -- fetching prod season list`);
            // Fetch results from the TXN/ProductionSeasons/Summary endpoint.
            // Docs here: https://www.tessituranetwork.com/REST_v151/TessituraService/HELP/API/GET_TXN_PRODUCTIONSEASONS_SUMMA.HTM
            // or here: https://tess-svc-test.fc.bard.edu/TessituraService/Help/Api/GET-TXN-ProductionSeasons-Summary_seasonIds_productionIds_ids
            try {
                const prodSeasons = await client('TXN/ProductionSeasons/Summary', { seasonIds: season });
                /* Returns an array of ProductionSeasonSummary objects. Example JSON:
                {
                    "Production": {
                        "Id": 42,
                        "Description": "SummerScape Opera"
                    },
                    "Id": 2507,
                    "Description": "2021 Opera - King Arthur",
                    "Season": {
                        "Id": 88,
                        "Description": "2021 Summer",
                        "FYear": 2022,
                        "Inactive": false
                    }
                } */

                console.log("Re-run this script with a prod_season=## arg to view performances in the production season.")
                prodSeasons.forEach(({ Id, Description }) => {
                    console.log(`${Id}: ${Description}`);
                });
                console.log("Re-run this script with a prod_season=## arg to view performances in the production season.")

            } catch (error) {
                console.error(error);
            }
            break;
        default:
            console.log("No valid parameters supplied -- fetching season list");
            // Fetch results from the ReferenceData/Seasons endpoint.
            // Docs here: https://www.tessituranetwork.com/REST_v151/TessituraService/HELP/API/GET_REFERENCEDATA_SEASONS_SUMMA.HTM
            // or here: https://tess-svc-test.fc.bard.edu/TessituraService/Help/Api/GET-ReferenceData-Seasons-Summary
            try {
                const seasons = await client('ReferenceData/Seasons');
                /* Returns an array of Season objects. Example JSON:
                {
                    "ConfirmationNoticeFormat": null,
                    "CreatedDateTime": "2021-05-19T14:12:21.673-04:00",
                    "CreateLocation": "multimaia_1",
                    "CreatedBy": "mkaufman",
                    "DefaultIndicator": true,
                    "Description": "2021 Summer",
                    "DisplayInSeasonOverview": true,
                    "EndDateTime": "2022-06-30T23:59:59-04:00",
                    "FYear": 2022,
                    "Id": 88,
                    "Inactive": false,
                    "UpdatedDateTime": "2021-05-19T14:12:21.673-04:00",
                    "UpdatedBy": "mkaufman",
                    "RenewalNoticeFormat": null,
                    "StartDateTime": "2021-05-01T00:00:00-04:00",
                    "SubscriptionFund1": null,
                    "SubscriptionFund2": null,
                    "Type": {
                        "Id": 5,
                        "Description": "Summer Events"
                    },
                    "YearlySeason": null,
                    "ControlGroup": {
                        "Description": "(Default Control Group)",
                        "Id": -1,
                        "Inactive": false
                    }
                } */

                function formatSeason(seasonObj) { return `${seasonObj.Id} - ${seasonObj.Description}`; }

                console.log("Re-run this script with a season=## arg to select a season.")

                const currentSeason = seasons.find(e => e.DefaultIndicator);
                console.log("Current default season is", formatSeason(currentSeason));

                seasons.sort((a, b) => Number(a.FYear) - Number(b.FYear)).forEach(e => {
                    console.log(formatSeason(e));
                });
                console.log("Current default season is", formatSeason(currentSeason));

                console.log("Re-run this script with a season=## arg to select a season.")
            } catch (error) {
                console.error(error);
            }
    }
})();
