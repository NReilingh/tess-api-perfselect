tess-api-perfselect

Example execution output:

```
direnv: loading ~/Repositories/tess-api-perfselect/.envrc
direnv: export +TESS_API_AUTH
nick@mjolnir tess-api-perfselect % 
nick@mjolnir tess-api-perfselect % 
nick@mjolnir tess-api-perfselect % node main.js 
Received input, season: undefined prod_season: undefined
No valid parameters supplied -- fetching season list
Re-run this script with a season=## arg to select a season.
Current default season is 88 - 2021 Summer
0 - System Default Season
1 - Default TR_SEASON
26 - 2003 Building Opening
27 - 2003 Conversion Season
2 - 2003 SummerScape/BMF
<snip>
83 - 2019-2020 TON
84 - 2019-2020 Season
85 - 2020 Summer
86 - FY21 Miscellaneous Products
87 - 2020-2021 Season
88 - 2021 Summer
Current default season is 88 - 2021 Summer
Re-run this script with a season=## arg to select a season.
nick@mjolnir tess-api-perfselect % 
nick@mjolnir tess-api-perfselect % 
nick@mjolnir tess-api-perfselect % node main.js season=88
Received input, season: 88 prod_season: undefined
season 88 supplied -- fetching prod season list
Re-run this script with a prod_season=## arg to view performances in the production season.
2449: 2021 Dance - Pam Tanowitz
2457: 2021 BMF - Boulanger (UPS)
2474: Justin Vivian Bond
2475: Black Roots Summer
2476: Most Happy
2493: 2021 BMF - Boulanger
2507: 2021 Opera - King Arthur
2508: 2021 Opera - King Arthur (UPS)
Re-run this script with a prod_season=## arg to view performances in the production season.
nick@mjolnir tess-api-perfselect % 
nick@mjolnir tess-api-perfselect % 
nick@mjolnir tess-api-perfselect % node main.js season=88 prod_season=2474
Received input, season: 88 prod_season: 2474
prod season 2474 supplied -- fetching performance list
Save the selected performance ID to the event.
2477: 0715JVB - Justin Vivian Bond
2478: 0716JVB - Justin Vivian Bond
2479: 0717JVB - Justin Vivian Bond
2480: 0718JVBR - Justin Vivian Bond Rain Date
Save the selected performance ID to the event.
nick@mjolnir tess-api-perfselect % 
nick@mjolnir tess-api-perfselect % 
nick@mjolnir tess-api-perfselect % 
```
